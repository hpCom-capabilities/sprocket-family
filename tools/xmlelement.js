var XML_CHAR_MAP = {
  '<': '&lt;',
  '>': '&gt;',
  '&': '&amp;',
  '"': '&quot;',
  "'": '&apos;'
};

XMLElement.XMLAttributes = XMLAttributes;

XMLElement.constants = {
  XML_NAMESPACE_ATTRIBUTE: 'xmlns'
};

XMLElement.encodeXML = function (text) {
  return text.replace(/[<>&"']/g, function (character) {
    return XML_CHAR_MAP[character];
  });
};

module.exports = XMLElement;

/**************************************************/
function XMLElement(name) {
  this.__name = name;
  this.__attrs = new XMLElement.XMLAttributes();
  this.__children = [];
  this.__contentText = '';
}

XMLElement.prototype.getName = function () {
  return this.__name;
};

XMLElement.prototype.setAttribute = function (attributeName, value) {
  this.__attrs.set(attributeName, value);
  return this;
};

XMLElement.prototype.setContentText = function (contentText) {
  this.__contentText = contentText;
  return this;
};

XMLElement.prototype.appendChild = function (childElement) {
  this.__children.push(childElement);
  return this;
};

XMLElement.prototype.toString = XMLElement.prototype.valueOf = function () {
  var elementString = '<' + this.__name + ' ' + this.__attrs;
  if (this.__children.length) {
    elementString += '>';
    for (var i = 0; i < this.__children.length; i++) {
      elementString += this.__children[i];
    }
    elementString += '</' + this.__name + '>';
  } else if (this.__contentText) {
    elementString += '>' + this.__contentText + '</' + this.__name + '>';
  } else {
    elementString += '/>';
  }
  return elementString;
};

function XMLAttributes() {
  this.__attrs = {};
}

XMLAttributes.prototype.set = function (name, value) {
  this.__attrs[name] = value;
};

XMLAttributes.prototype.get = function (name) {
  return this.__attrs[name] || null;
};

XMLAttributes.prototype.remove = function (name) {
  if (this.__attrs[name]) {
    delete this.__attrs[name];
  }
};

XMLAttributes.prototype.toString = XMLAttributes.prototype.valueOf = function () {
  var attrsStrList = [],
      attributeName;
  for (attributeName in this.__attrs) {
    attrsStrList.push(attributeName + '="' + this.__attrs[attributeName] + '"');
  }
  return attrsStrList.join(' ');
};
