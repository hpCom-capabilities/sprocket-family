var gulp = require('gulp');
var clean = require('gulp-clean');
var sass = require('gulp-sass');
var webserver = require('gulp-webserver');
var minify = require('gulp-minify');
var concat = require('gulp-concat');
const browserSync = require('browser-sync');
const replace = require('gulp-replace');
const fs = require('fs');
const XMLElement = require('./tools/xmlelement');
const pd = require('pretty-data').pd;
const autoprefixer = require('gulp-autoprefixer');


gulp.task('sass', function () {
  gulp.src('./app/sass/application.scss')
    .pipe(sass())
    .pipe(concat('sprocket-family.css'))
    .pipe(autoprefixer({browsers: ['last 4 versions']}))
    .pipe(gulp.dest('./dist/style'));
  gulp.src('./app/sass/fontfaces.scss')
    .pipe(sass())
    .pipe(concat('flex2-fontfaces.css'))
    .pipe(gulp.dest('./dist/style'));
  gulp.src('./app/sass/fontfaces-cy.scss')
    .pipe(sass())
    .pipe(concat('flex2-fontfaces-cy.css'))
    .pipe(gulp.dest('./dist/style'));
});

gulp.task('js', function () {
    return gulp.src(
        [
            './app/js/jquery.js',
            './app/js/slick.min.js',
            './app/js/metrics.js',
            './app/js/components/overlay.js',
            './app/js/components/overlayYoutube.js',
            './app/js/components/overlayBrightcove.js',
            './app/js/components/overlayReseller.js',
            './app/js/base.js',
            './app/js/sticky-bar.js',
            './app/js/components/scroll-to-anchor.js',
            './app/js/section-ambient-video.js',
            './app/js/section-go-from.js',
            './app/js/section-around-world.js',
            './app/js/section-meets-press.js',
            './app/js/section-reinvent-memories.js',
            './app/js/section-app-experience.js',
            './app/js/section-already-have.js',
            './app/js/section-club-is-back.js',
            './app/js/section-every-pocket.js',
            './app/js/section-gradient-section.js',
            './app/js/section-meet-family.js',
            './app/js/section-sprocket-accessories.js',
            './app/js/section-instant-shareable.js',
            './app/js/section-support.js'
        ]
    )
        .pipe(concat('sprocket-family-min.js'))
        .pipe(gulp.dest('./dist/script/'));
});

gulp.task('translate', function() {
  var translations = JSON.parse(fs.readFileSync('./translations/index.json'));

return gulp.src(['./app/*.html'])
  .pipe(replace(/##([\w-]+)##/gm, function (match, key) {
    var translationFound = typeof translations[key] !== 'undefined',
      value = translationFound ? translations[key].value : '';

    if (!translationFound) {
      console.warn('No translation for key \'' + key + '\' was found');
    }

    return value;
  }))
  .pipe(gulp.dest('./dist/', { overwhite: true }));
});


// gulp.task('compress', function() {
//   gulp.src('./dist/script/sprocket-family-min.js')
//     .pipe(minify({
//       ext:{
//         src:'.js',
//         min:'.min.js'
//       }
//     }))
//     .pipe(gulp.dest('./dist/script/'))
// });


gulp.task('serve', ['sass', 'js', 'translate'/*, 'compress'*/], function(){
  gulp.watch('./app/sass/*.scss', ['sass']);
  gulp.watch('./app/js/**/*.js', ['js']);
  gulp.watch('./app/*.html', ['translate']);

  // gulp.run('compress');

  // gulp.src(['./app/fonts/**/*'])
  //   .pipe(gulp.dest('./dist/fonts'));

  gulp.src(['./app/images/**/*'])
    .pipe(gulp.dest('./dist/images'));

  gulp.src(['./app/i/**/*'])
  .pipe(gulp.dest('./dist/style/i'));

  gulp.src(['./app/fonts/hps/*'])
    .pipe(gulp.dest('./dist/fonts/hps'));  

  browserSync({
    notify: false,
    port: 9002,
    startPath: '/',
    server: {
      baseDir: ['dist']
    }
  });
});

//task for generate file translations for tridion
gulp.task('generate-translation-xml', () => {
  const TRANSLATE_ITEM_TAG = 'translate_item',
  KEY_TAG = 'key',
  VALUE_TAG = 'value',
  INPUT_INSTRUCTIONS_TAG = 'input_instructions',
  XML_NAMESPACE_ATTRIBUTE = 'xmlns',
  XHTML_NAMESPACE = 'http://www.w3.org/1999/xhtml',
  TRANSLATABLE_ITEMS_NAMESPACE = 'http://www.hp.com/Tridion/epo/Schemas/TranslatableItemsOpenTemplate',

  translations = JSON.parse(fs.readFileSync('./translations/index.json')),
  destFile = './dist/translations.xml';

  fs.stat(destFile, function (error, stat) {
    if (error === null) {
      fs.unlink(destFile);
    }

    var xmlString = createTranslatedContent();
    fs.writeFileSync(destFile, xmlString);
  });

  function createTranslatedContent() {
    var NASA_LABEL_PREFIX = '';

    var item,
      itemName,
      translatedItems = new XMLElement('translated_items');

    translatedItems.setAttribute(XMLElement.constants.XML_NAMESPACE_ATTRIBUTE, TRANSLATABLE_ITEMS_NAMESPACE);
    for (itemName in translations) {
      if (itemName.indexOf(NASA_LABEL_PREFIX) === -1) {
        continue;
      }

      item = createTranslateItem(itemName, translations[itemName].value, translations[itemName].instructions);
      translatedItems.appendChild(item);
    }
    console.log(pd.xml(translatedItems.toString()));
    return pd.xml(translatedItems.toString());
  }

  function createTranslateItem(key, value, instructions) {
    var translateItem, keyElement, valueElement, inputInstructionsElement;

    keyElement = createKeyElement(key);
    valueElement = createValueElement(value);
    inputInstructionsElement = createInputInstructionsElement(instructions);

    translateItem = new XMLElement(TRANSLATE_ITEM_TAG)
      .appendChild(keyElement)
      .appendChild(inputInstructionsElement)
      .appendChild(valueElement);

    return translateItem;
  }

  function createKeyElement(key) {
    var keyElement = new XMLElement(KEY_TAG);
    keyElement.setContentText(key);
    return keyElement;
  }

  function createValueElement(value) {
    var valueElement = new XMLElement(VALUE_TAG);
    valueElement.setContentText(prepareValue(value));
    return valueElement;
  }

  function prepareValue(value) {
    var newValue =  value.replace(/(<[a-zA-Z0-9]+)([^>]*>)/g, function (match, head, tail) {
      return head + ' ' + XML_NAMESPACE_ATTRIBUTE + '="' + XHTML_NAMESPACE + '" ' + tail;
    });
    newValue = newValue.replace(/&(?!(nbsp;)|(quot;)|(lt;)|(gt;)|(copy)|(amp))/g, '&amp;');
    newValue = newValue.replace(/&nbsp;/g, '&#160;');
    return newValue;
  }

  function createInputInstructionsElement(instructions) {
    var inputInstructionsElement = new XMLElement(INPUT_INSTRUCTIONS_TAG);
    inputInstructionsElement.setContentText(XMLElement.encodeXML(instructions));
    return inputInstructionsElement;
  }
});

gulp.task('clean-scripts', function () {
  return gulp.src('dist', {read: false})
    .pipe(clean());
});