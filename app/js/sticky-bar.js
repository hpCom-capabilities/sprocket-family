$(function() {
  var stickyBar = {
      stickyBar: $('.sticky-section'),

      addListeners: function () {
          var self = this;

          $(window).on('scroll', function() {
              var startPoint = $('.go-from').get(0).getBoundingClientRect().top;

              if (startPoint <= 0 ) {
                self.showBar();
              } else {
                self.hideBar();
            }
          });
      },

      setupBar: function () {
          if (!this.stickyBar.find('.sticky-wrapper a.button').length) {
              this.stickyBar.addClass('no-cta-button');
          }
      },

      showBar: function () {
          this.stickyBar.addClass('show-bar');
      },

      hideBar: function () {
          this.stickyBar.removeClass('show-bar');
      },

      init: function () {
          this.setupBar();
          this.addListeners();
      }
  };

  stickyBar.init();
});