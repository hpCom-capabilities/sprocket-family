$(function () {
  var isRtl = $('html').attr('dir') == 'rtl';
  var resizeTimeout,
    everyPocketSlider = false;
  $(window).on('resize', function () {
    clearTimeout(resizeTimeout);
    resizeTimeout = setTimeout(function () {
      checkSliderInitialization();
    }, 100);
  });

  function checkSliderInitialization() {
    if (window.innerWidth > 800) {
      if (everyPocketSlider) {
        everyPocketSlider = false;
        $('section.every-pocket .bottom-group').slick('unslick');
      }
    } else {
      if (!everyPocketSlider) {
        everyPocketSlider = true;
        initEveryPocketSlider();
      }
    }
  }
  checkSliderInitialization();

  function initEveryPocketSlider() {
    $('section.every-pocket .bottom-group').slick({
      dots: true,
      infinite: true,
      speed: 300,
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      rtl: isRtl
    });
  }
});