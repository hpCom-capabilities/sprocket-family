var Overlay = (function () {

    var Overlay = function ($el) {
        this.$el = $el;
        this.type = $el.attr('data-overlay-type');
        this.setupEvents();
    };

    Overlay.prototype = {
        setupEvents: function () {
            this.$el.find('.close-button').on('click', this.close.bind(this));
            this.$el.find('.modal-mask').on('click', function (e) {
              if ($(e.target).hasClass('modal-mask') ||
                  $(e.target).hasClass('content-wrapper')){
                  this.close();
              }
            }.bind(this));
        },
        close: function () {
            if (this.type === 'youtube') {
                overlayYoutube.destroy();
            }
            if (this.type === 'brightcove') {
                overlayBrightcove.destroy();
            }
            if (this.type === 'reseller') {
                overlayReseller.destroy();
            }
            this.cleanContainer();
            this.$el.removeClass('opened');
        },
        cleanContainer: function () {
            this.$el.find('.modal-mask').removeClass(this.theme)
            this.$el.removeClass('square-video')
        },
        open: function (options) {
            if (options && options.theme) {
                this.theme = options.theme;
                this.$el.find('.modal-mask').addClass(options.theme)
            }
            if (options && options.squareVideo === 'true') {
                this.$el.addClass('square-video')
            }
            if (options && this.type === 'youtube') {
                if (options.videoId) {
                    this.$el.find('.player-container').attr('id', 'player-' + options.videoId);
                    overlayYoutube.showPlayer({
                        id: 'player-' + options.videoId,
                        videoId: options.videoId
                    })
                }
            }
            if (options && this.type === 'brightcove') {
                if (options.callback) {
                    options.callback(options);
                }
            }
            if (this.type === 'reseller') {
                overlayReseller.showProduct();
            }
            this.$el.addClass('opened');
        }
    };

    return Overlay;
})();