var overlayYoutube = (function () {
    return {
        apiLoaded: false,
        overlay: null,
        activePlayer: null,
        states: {
            UNSTARTED: -1,
            ENDED: 0,
            PLAYING: 1,
            PAUSED: 2,
            BUFFERING: 3,
            CUED: 5
        },
        init: function () {
            this.loadApi();
            this.buildOverlay();
            this.setupEvents();
        },
        loadApi: function () {
            var tag = document.createElement('script');
            tag.src = "//www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

            window.onYouTubeIframeAPIReady = function () {
                this.apiLoaded = true;
            }.bind(this);
        },
        buildOverlay: function () {
            var $container = $('#youtube-video-overlay');
            this.overlay = $container.length ? new Overlay($container) : null;
        },
        setupEvents: function () {
            $('.overlay-trigger').each(function (index, el) {
                if ($(el).attr('data-overlay-type') === 'video') {
                    $(el).bind('click', this.openOverlay.bind(this));
                }
            }.bind(this));
        },
        openOverlay: function (e) {
            var $button = $(e.target).closest('.overlay-trigger');
            this.overlay.open({
                videoId: $button.attr('data-youtube-video-id') || null,
                theme: $button.attr('data-overlay-theme') || null,
                squareVideo: $button.attr('data-video-square') || null
            });
        },
        showPlayer: function (playerInfo) {
            if (!playerInfo.id || !playerInfo.videoId) {
                return;
            }
            if (this.apiLoaded) {
                this.activePlayer = new YT.Player(playerInfo.id, {
                    height: playerInfo.height || '360',
                    width: playerInfo.width || '640',
                    videoId: playerInfo.videoId,
                    playerVars: {
                        enablejsapi: 1,
                        origin: document.location.origin
                    },
                    events: {
                        'onReady': this.onPlayerReady,
                        'onStateChange': this.onPlayerStateChange.bind(this)
                    }
                });
            }
        },
        onPlayerReady: function (event) {
            event.target.playVideo();
        },
        onPlayerStateChange: function (event) {
            var videoDurationInSeconds = Math.floor((event.target.getDuration() - 0.01) || 0),
                videoQueuePointInSeconds = Math.round(event.target.getCurrentTime() || 0),
                videoTitle = event.target.getVideoData().title;
            try {
                switch (event.data) {
                    case this.states.UNSTARTED:
                        console.log('** Metrics: YT UNSTARTED **', 'open' + ', ' + videoTitle + ', ' + videoDurationInSeconds);
                        trackVideoMetrics('open', videoTitle, videoDurationInSeconds);
                        break;
                    case this.states.PLAYING:
                        console.log('** Metrics: YT PLAYING **', 'play' + ',' + videoTitle + ', ' + videoQueuePointInSeconds + ' of ' + videoDurationInSeconds);
                        trackVideoMetrics('play', videoTitle, videoQueuePointInSeconds);
                        break;
                    case this.states.PAUSED:
                        console.log('** Metrics: YT PAUSED **', 'stop' + ',' + videoTitle + ', ' + videoQueuePointInSeconds + ' of ' + videoDurationInSeconds);
                        trackVideoMetrics('stop', videoTitle, videoQueuePointInSeconds);
                        break;
                    case this.states.ENDED:
                        console.log('** Metrics: YT ENDED **', 'complete' + ',' + videoTitle);
                        trackVideoMetrics('complete', videoTitle);
                        break;
                }
            } catch (e) {
                console.log('** Metrics FAILED ** due to: ' + e.message);
            }
        },
        destroy: function () {
            if (this.activePlayer) {
                var videoDurationInSeconds = Math.floor((this.activePlayer.getDuration() - 0.01) || 0),
                    videoQueuePointInSeconds = Math.round(this.activePlayer.getCurrentTime() || 0),
                    videoTitle = this.activePlayer.getVideoData().title;
                try {
                    console.log('** Metrics: CLOSED **', 'close' + ',' + videoTitle + ', ' + videoQueuePointInSeconds + ' of ' + videoDurationInSeconds);
                    trackVideoMetrics('close', videoTitle, videoQueuePointInSeconds);
                } catch (e) {
                    console.log('** Metrics FAILED ** due to: ' + e.message);
                }
                this.activePlayer.destroy();
                this.activePlayer = null;
                $('#youtube-video-overlay').find('.player-container').empty().attr('id', '');
            }
        }
    }
})();