var overlayBrightcove = (function () {
    return {
        player: null,
        overlay: null,
        init: function () {
            this.buildOverlay();
            this.setupEvents();
        },
        buildOverlay: function () {
            var $container = $('#brightcove-video-overlay');
            this.overlay = $container.length ? new Overlay($container) : null;
        },
        setupEvents: function () {
            $('.overlay-trigger').each(function (index, el) {
                if ($(el).attr('data-overlay-type') === 'video') {
                    $(el).bind('click', this.openOverlay.bind(this));
                }
            }.bind(this));
        },
        openOverlay: function (e) {
            var $button = $(e.target).closest('.overlay-trigger');
            this.overlay.open({
                callback: this.buildPlayer.bind(this),
                accountId: $button.attr('data-bc-account') || null,
                playerId: $button.attr('data-bc-player') || null,
                videoId: $button.attr('data-bc-video-id') || null,
                theme: $button.attr('data-overlay-theme') || null,
                squareVideo: $button.attr('data-video-square') || null
            });
        },
        buildPlayer: function (playerData) {
            if (!playerData.videoId || !playerData.playerId || !playerData.accountId) {
                return;
            }
            $('#brightcove-video-overlay').find('.player-container').html(this.getVideoHtml(playerData));

            var s = document.createElement('script');
            s.src = "//players.brightcove.net/" + playerData.accountId + "/" + playerData.playerId + "_default/index.min.js";
            document.body.appendChild(s);
            s.onload = this.onPlayerReady.bind(this);
        },
        getVideoHtml: function (data) {
            var $video = $('<video>', {
                id: 'bc-player',
                'data-video-id': data.videoId,
                'data-account': data.accountId,
                'data-player': data.playerId,
                'data-embed': 'default',
                controls: true,
                class: 'video-js',
                style: 'width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;'
            });
            return $video[0].outerHTML;
        },
        onPlayerReady: function () {
            this.player = videojs('bc-player');
            this.player.play();
            $('#bc-player').css('opacity', 1)
        },
        destroy: function () {
            if (this.player) {
                this.player.pause();
                this.player = null;
            }
            $('#brightcove-video-overlay').find('.player-container').empty();
        }
    }
})();