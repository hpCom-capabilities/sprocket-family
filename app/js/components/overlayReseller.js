var overlayReseller = (function() {
    return {
        overlay: null,
        activeProduct: null,
        activeColor: null,
        data: {},
        init: function() {
            this.buildOverlay();
            this.setupEvents();
        },
        buildOverlay: function() {
            this.$el = $('#reseller-with-colors-overlay');
            this.overlay = this.$el.length ? new Overlay(this.$el) : null;
            this.data = _reseller || {};
        },
        setupEvents: function() {
            $('.overlay-trigger').each(function(index, el) {
                if ($(el).attr('data-overlay-type') === 'reseller') {
                    $(el).bind('click', this.openOverlay.bind(this));
                }
            }.bind(this));
        },
        openOverlay: function(e) {
            var $button = $(e.target).closest('.overlay-trigger');
            this.activeProduct = $button.attr('data-product') || null;
            if (this.activeProduct && this.data) {
                this.overlay.open();
            }
        },
        showProduct: function() {
            if (this.data[this.activeProduct]) {
                this.$el.find('.text-block .title').html(this.data[this.activeProduct].title || '');
                this.$el.find('.text-block .description').html(this.data[this.activeProduct].description || '');
                if (this.data[this.activeProduct].description2) {
                    this.$el.find('.text-block .description-2').html(this.data[this.activeProduct].description2 || '').addClass('active');
                }
                this.checkColors();
                this.$el.find('.image-block').html(this.getImgHtml());
                this.$el.find('.shop-logos h2').html(this.data.storeHeader);
                this.showStores();
            }
        },
        checkColors: function() {
            this.$el.find('.color-dots').empty();
            if (this.activeProduct &&
                this.data[this.activeProduct] &&
                this.data[this.activeProduct].colors) {
                $.each(this.data[this.activeProduct].colors, function(ind, color) {
                    if (color.hex.trim() != 'false') {
                        if (this.activeColor === null) {
                            this.activeColor = ind;
                        }
                        this.$el.find('.color-dots').append(this.getDotHtml(ind))
                    }
                }.bind(this));
                if (this.activeColor !== null) {
                    this.$el.find('.text-block .color').html(this.getColorHtml()).addClass('active');
                    this.$el.find('.color-dots').addClass('active');
                    $('.color-dot').on('click', this.changeActiveColor.bind(this));
                }
            }
        },
        escapeAmpersand: function(string) {
            return string.replace(/&amp;/g, '&');
        },
        showStores: function() {
            $('.logos a').removeClass('active');
            var storeData = {};
            if (this.activeColor !== null) {
                storeData = this.data[this.activeProduct].colors[this.activeColor].stores
            } else {
                storeData = this.data[this.activeProduct].stores || {};
            }
            $.each(storeData, function(store, href) {
                if (href.trim() != 'false') {
                    var $storeLink = this.$el.find('.' + store);
                    $storeLink.addClass('active');
                    href = this.escapeAmpersand(href);
                    $storeLink.attr('href', href);
                }
            }.bind(this));
        },
        changeActiveColor: function(e) {
            this.activeColor = $(e.target).closest('.color-dot').attr('data-index');
            this.$el.find('.image-block').html(this.getImgHtml());
            $('.color-dot').removeClass('active');
            $(e.target).closest('.color-dot').addClass('active');
            this.$el.find('.text-block .color').html(this.getColorHtml());
            this.showStores();
        },
        getImgHtml: function() {
            var imgData = null;
            if (this.activeColor !== null) {
                imgData = this.data[this.activeProduct].colors[this.activeColor].image
            } else {
                imgData = this.data[this.activeProduct].image || null;
            }
            var $img = $('#hidden-image-section').find("[data-image-title='" + imgData + "']");
            if ($img.length) {
                return $img.clone()[0].outerHTML;
            }
            return '';
        },
        getColorHtml: function() {
            var $span = $('<span>', {
                class: 'color-name',
            });
            $span.text(this.data[this.activeProduct].colors[this.activeColor].name);
            return this.data.colorHeader + ': ' + $span[0].outerHTML;
        },
        getDotHtml: function(index) {
            var $li = $('<li>');
            var $a = $('<a>', {
                class: 'color-dot',
                'data-index': index
            });
            $a.html('<div class="circle border"><div class="circle border-space"><div class="circle colored" style="background:' + this.data[this.activeProduct].colors[index].hex + '"></div></div></div>');
            if (this.activeColor === index) {
                $a.addClass('active');
            }
            $li.html($a);
            return $li[0].outerHTML;
        },
        destroy: function() {
            this.activeColor = null;
            this.activeProduct = null;
            this.$el.find('.text-block .description-2').removeClass('active');
            this.$el.find('.text-block .color').removeClass('active');
            this.$el.find('.color-dots').removeClass('active');
        }
    }
})();