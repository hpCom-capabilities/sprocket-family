$(function () {
	var scrollToAnchorTrigger = $('[data-anchor-trigger]');
	var SCROLL_SPEED = 1200;

	if (scrollToAnchorTrigger.length) {
		scrollToAnchorTrigger.each(function (ind, elem) {
			var triggerBtn = $(elem);
			var anchorValue = triggerBtn.attr('data-anchor-trigger');
			var anchorElement = $('[data-anchor^="' + anchorValue + '"]');

			if (anchorElement.length) {
				addScrollToElemFunctionality(triggerBtn, anchorElement);
			}
		})
	}

	function addScrollToElemFunctionality(triggerBtn, anchorElement) {
		triggerBtn.on('click', function (e) {
			e.preventDefault();
			$('html, body').animate({
		        scrollTop: anchorElement.offset().top
		    }, SCROLL_SPEED);
		})
	}
});