$(function() {

    var resizeTimeout,
        everyPocketSlider = false;
    var isRtl = $('html').attr('dir') == 'rtl';

    $(window).on('resize', function() {
        clearTimeout(resizeTimeout);
        resizeTimeout = setTimeout(function() {
            checkItems();
        }, 100);
    });

    function checkSliderInitialization() {        
        var items = $('section.sprocket-accessories .slider .cell'),
            length = items.length;

        if (length == 3 || length == 2) {
            if (window.innerWidth > 1024) {
                if (everyPocketSlider) {
                    everyPocketSlider = false;
                    $('section.sprocket-accessories .slider').slick('unslick');
                }
            } else {
                if (!everyPocketSlider) {
                    everyPocketSlider = true;
                    initAccessoriesSlider();
                }
            }
        } else if (length > 1) {
          if (!everyPocketSlider) {
              everyPocketSlider = true;
              initAccessoriesSlider();
          }
        }

    }

    function checkItems() {
        var items = $('section.sprocket-accessories .slider .cell'),
            length = items.length;

        if (length > 1) {
            checkSliderInitialization();
        }
    }

    function initItemsWidth() {
        var itemsNew = $('section.sprocket-accessories .slider .cell'),
            length = itemsNew.length;

        itemsNew.each(function() {
            var cell = $(this);
            if (length == 3 || length > 3) {
                cell.addClass('width-33');
            } else if (length == 2) {
                cell.addClass('width-40');
            } else if (length == 1) {
                cell.addClass('width-100');
            }
        });        
    }


    function initAccessoriesSlider() {
        $('section.sprocket-accessories .slider').slick({
            dots: false,
            infinite: true,
            speed: 500,
            arrows: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            rtl: isRtl,
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        dots: true
                    }
                }
            ]
        });

    }

    initItemsWidth();
    checkItems();
});