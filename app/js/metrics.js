var metrics = {

  init : function() {
    var _self = this;

    this.languageCode = document.documentElement.lang.split("-")[0];//ja||us
    this.countryCode =  document.documentElement.lang.split("-")[1];//jp||en
    this.bu = $('[name="bu"]').attr('content').toLowerCase();
    this.simple_title = $('[name="simple_title"]').attr('content');

    $(document).on('click', '.metrics-tracker, .metalocator-buy-wrapper', function (e) {
      _self.sendMetric($(e.currentTarget));
    });

    document.addEventListener('click', function (e) {
      var parentSpanMetalocator = $(e.target).parent('.metalocator-buy-wrapper');
      if(parentSpanMetalocator.length){
        _self.sendMetric(parentSpanMetalocator);
      }
    }, true); //catch click event on the interception phase

  },
  sendMetric: function(elementToTrack){

    var metricsType       = '',
      metricValue       = "",
      metricLinkTitle = $(elementToTrack).attr('data-metric-title');


    metricValue = (this.bu + '/' + this.countryCode + '/' + this.languageCode + '/' + this.simple_title + '/' + metricLinkTitle);
    metricsType = elementToTrack.attr('data-metrics-link-type') || 'link';

    //console.log('metricValue',metricValue);

    if ( metricValue ) {
      try {
        trackMetrics('newLink', { name :  metricValue, type : metricsType });
      } catch (excpt) {
        console.log("Metrics error:");
        console.log(excpt);
      };
    } else {
      console.log('trackMetrics has NOT beet called cause data-metric-title param is absent');
    }

    return true;
  },
};


$(document).ready(function(){
  metrics.init();
});
