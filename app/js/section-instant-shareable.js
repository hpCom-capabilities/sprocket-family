$(function() {
    var resizeTimeout,
        everyPocketSlider = false;
    var isRtl = $('html').attr('dir') == 'rtl';

    $(window).on('resize', function() {
        clearTimeout(resizeTimeout);
        resizeTimeout = setTimeout(function() {
            moveOR();
            checkItems();
        }, 100);
    });

    function checkSliderInitialization() {
        if (window.innerWidth > 800) {
            if (everyPocketSlider) {
                everyPocketSlider = false;
                $('section.instant-shareable .three-items').slick('unslick');
            }
        } else {
            if (!everyPocketSlider) {
                everyPocketSlider = true;
                initInstantShareableSlider();
            }
        }
    }

    function moveOR() {
        var items = $('section.instant-shareable .three-items .item'),
            separator = $('section.instant-shareable .desktop-only'),
            section = $('section.instant-shareable'),
            resizeTimeout;
                    
        if (items.length > 1 && separator.length) {
            if (window.innerWidth > 800) {
                clearTimeout(resizeTimeout);
                resizeTimeout = setTimeout(function() {
                    items.first().parents('.width').after(separator);
                }, 300);                
            } else {
                section.append(separator);
            }
        }

        if(items.length == 1 && separator.length) {
        	separator.remove();
        }

    }

    function checkItems() {
        var items = $('section.instant-shareable .three-items .item'),
            length = items.length;

        if (length > 1) {
            checkSliderInitialization();
        }
    }

    function initItemsWidth() {
        var items = $('section.instant-shareable .three-items .item');

        for (var i = 0; i < items.length; i++) {
            var text = $(items[i]).find('.text-wrapper').text();
            if (!$.trim(text) || text.indexOf('##') > -1) {
                $(items[i]).remove();
                continue;
            }
        }

        var itemsNew = $('section.instant-shareable .three-items .item'),
            length = itemsNew.length;

        itemsNew.each(function() {
            var parent = $(this).parent('.width');
            if (length == 3) {
                parent.addClass('width-33');
            } else if (length == 2) {
                parent.addClass('width-50');
            } else if (length == 1) {
                parent.addClass('width-100');
            }
        });
    }

    moveOR();
    initItemsWidth();
    checkItems();

    function initInstantShareableSlider() {
        $('section.instant-shareable .three-items').slick({
            dots: true,
            infinite: true,
            speed: 300,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            rtl: isRtl
        });
    }
});