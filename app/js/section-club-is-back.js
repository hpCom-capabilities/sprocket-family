$(function () {
  var isRtl = $('html').attr('dir') == 'rtl';
  $('.club-is-back .slider').slick({
    dots: true,
    infinite: false,
    speed: 500,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    rtl: isRtl
  });
});