$(function () {
    var initAppExperiencedSlider = {
        sectionTextWrapper: $('section.app-experience .text-wrapper'),
        slider: $('section.app-experience .slider'),
        slickDescription: $('<div class="slick-description"/>'),
        cellsDescription: $('section.app-experience .description'),
        cellMaxWidth: 0,
        slickDescMaxWidth: 0,
        slickDescXPosition: 0,
        isRtl: $('html').attr('dir') == 'rtl',

        createSlickDescription: function () {
            this.sectionTextWrapper.append(this.slickDescription);

            this.cellsDescription.each(function (ind, cell) {
                this.slickDescription.append($(cell).clone(true))
            }.bind(this));

            this.setCellsEqualWidth();
        },

        setCellsEqualWidth: function () {
            this.cellMaxWidth = 0;
            var cellsCount = 0;

            this.slickDescription.find('.description').each(function (ind, cell) {
                var cellWidth = Number($(cell).width());
                cellsCount++;
                this.cellMaxWidth = this.cellMaxWidth > Math.floor(cellWidth) ? this.cellMaxWidth : Math.floor(cellWidth) + 2;
            }.bind(this));

            this.slickDescription.find('.description').css('width', this.cellMaxWidth);

            this.setSlickDescriptionWidth(this.cellMaxWidth * cellsCount);
            this.addSlickDescWrapper(this.cellMaxWidth);
        },

        setSlickDescriptionWidth: function (widthValue) {
            this.slickDescription.css({
                "width": widthValue,
                "transform": "translate3d(0, 0, 0)",
                "transition": "transform 300ms ease" // THE SAME VALUE FOR SLICK speed: 300
            });
            this.slickDescMaxWidth = widthValue;
        },

        addSlickDescWrapper: function (width) {
            this.slickDescription.wrap('<div class="slick-desc-wrapper"></div>');
            this.slickDescWrapper = $('section.app-experience .slick-desc-wrapper');
            this.slickDescWrapper.css('width', width);
        },

        changeSlickDescPosition: function (xValue) {
            var translateValue = this.isRtl ? 'translate3d(' + xValue + 'px, 0, 0)' : 'translate3d(-' + xValue + 'px, 0, 0)';
            this.slickDescription.css('transform', translateValue);
        },

        setupEvents: function () {
            $('.app-experience .slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                if (nextSlide === 0) {
                    this.slickDescXPosition = 0;
                } else {
                    this.slickDescXPosition = nextSlide * this.cellMaxWidth;
                }
                this.changeSlickDescPosition(this.slickDescXPosition);
            }.bind(this));
        },

        initializeSlick: function () {
            var self = this;
            $('.app-experience .slider').slick({
                dots: true,
                infinite: true,
                speed: 300, // THE SAME VALUE FOR SLICK DESCRIPTION: 300
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                rtl: self.isRtl
            });
        },

        init: function () {
            this.createSlickDescription();
            this.setupEvents();
            this.initializeSlick();
        }
    };

    initAppExperiencedSlider.init();
});