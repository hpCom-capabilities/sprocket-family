$(function () {
    var isRtl = $('html').attr('dir') == 'rtl';
    function initAroundWorldSlider() {
      $('section.around-world .slider').slick({
          dots: true,
          infinite: true,
          speed: 500,
          arrows: true,
          slidesToShow: 3,
          slidesToScroll: 3,
          rtl: isRtl,
          responsive: [
              // {
              //     breakpoint: 1024,
              //     settings: {
              //         slidesToShow: 2,
              //         slidesToScroll: 2
              //     }
              // },
              {
                  breakpoint: 800,
                  settings: {
                      slidesToShow: 1,
                          slidesToScroll: 1,
                          arrows: false
                  }
              }
          ]
      });
    }
    initAroundWorldSlider();
});