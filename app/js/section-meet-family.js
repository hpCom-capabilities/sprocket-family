$(function () {

  checkActiveProductImg();
  initItemsWidth();
  addSliderBehavior();

  function checkActiveProductImg() {
    var items = $('section.meet-family .three-items .item');
    items.each(function (ind, item) {
      var imgFirst = $(item).find('.image img:first');
      if (!imgFirst.hasClass('active')) {
        imgFirst.addClass('active');
      }
    })
  }

  function initItemsWidth() {
    var items = $('section.meet-family .three-items .item');       

    for (var i = 0; i < items.length; i++) {    
      var text = $(items[i]).find('.text-wrapper').text();      
      if (!$.trim(text) || text.indexOf('##') > -1) {
        $(items[i]).remove();
        continue;
      } 
    }

    var itemsNew = $('section.meet-family .three-items .item'),
         length = itemsNew.length;

    itemsNew.each(function () {
      var parent = $(this).parent('.width');        
      if (length == 3) {
        parent.addClass('width-33');
      } else if (length == 2) {
        parent.addClass('width-50');
      } else if (length == 1) {
        parent.addClass('width-100');
      } 
    });           
  }

  function addSliderBehavior(){
    var meetFamilySection = $('section.meet-family');
    if (meetFamilySection.length) {
      if (meetFamilySection.hasClass('js-slider')) {
        createSlickSlider(meetFamilySection);
      } else {
        createCssSlider(meetFamilySection);
      }
    }

    function createSlickSlider(section){
      var sliderBlock = section.find('.three-items');
      var resizeTimeout;
      var sliderIsInit = false;
      var isRtl = $('html').attr('dir') == 'rtl';

      checkSliderInitialization();

      $(window).on('resize', function () {
        clearTimeout(resizeTimeout);
        resizeTimeout = setTimeout(function () {
          checkSliderInitialization();
        }, 100);
      });

      function checkSliderInitialization() {
        if (window.innerWidth > 800) {
          destroySlider();
        } else {
          initMeetFamilySlider();
        }
      }

      function destroySlider() {
        if (sliderIsInit) {
          sliderIsInit = false;
          sliderBlock.slick('unslick');
        }
      }

      function initMeetFamilySlider() {
        if (!sliderIsInit) {
          sliderIsInit = true;
          sliderBlock.slick({
            dots: true,
            infinite: true,
            speed: 300,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            rtl: isRtl
          });
        }
      }
    }

    function createCssSlider(section){
      var itemsWidth = section.find('.width');
      if (itemsWidth.length && itemsWidth.length > 1) {
        itemsWidth.wrapAll('<div class="items-wrapper"/>');
      }
    }
  }

  var selectProductByColorCtrl = {
      threeItems: $('.meet-family .three-items .width'),

      init: function () {
          this.initProducts();
      },
      initProducts: function() {
          var self = this;
          this.threeItems.each(function (ind, product) {
              var productItem = new ProductItem($(product));
              self.updateColorsView(productItem);
          })
      },
      updateColorsView: function (productItem) {
          if (productItem && productItem.colors ) {
              productItem = this.checkProperColorsData(productItem);
              if ( productItem.colors.length) {
                  this.updateColorsByData(productItem);
              } else {
                  this.removeColorList(productItem.colors);
                  this.removeColorName(productItem.colorName);
              }
          }
      },
      checkProperColorsData: function (product) {
        var self = this;
        var properColorProperty = function(index, color) {
          var $color = $(color);
          if (self.dataNotExist($color)) {
            self.removeUnusedElems(product, $color.attr('data-color-name'));
            return false;
          } else {
            return true;
          }
        };

        product.colors = product.colors.filter(properColorProperty)

        return product;
      },
      dataNotExist: function (color) {
        return !( this.isProperHexColor(color) && this.isHasName(color) && this.isProperHref(color) );
      },
      isProperHexColor: function (color) {
        return color.attr('data-hex-color') && color.attr('data-hex-color').indexOf('#') != -1;
      },
      isHasName: function (color) {
        return color.attr('data-color-name');
      },
      isProperHref: function (color) {
        return color.attr('data-href') && color.attr('data-href') != 0;
      },
      removeUnusedElems: function (product, colorName) {
        this.removeElementByName(product.imgs,colorName);
        this.removeElementByName(product.colors,colorName);
        this.removeElementByName(product.shopNowBtns,colorName);
      },
      removeElementByName: function (elements, name) {
        var filterName = function (index, elem) {
          return $(elem).attr('data-color-name') == name;
        }
        elements.filter(filterName).remove();
      },
      updateColorsByData: function (productItem) {
          this.setColorsByData(productItem.colors);
          this.addEvents(productItem);
      },
      setColorsByData: function (colorList) {
          colorList.each(function (ind, liItem) {
              var span = $(liItem).find('span');
              span.css('background', $(liItem).attr('data-hex-color'));
          })
      },
      removeColorList: function (productColors) {
          productColors.parent('ul').remove();
      },
      removeColorName: function (productColorName) {
          productColorName.remove();
      },
      addEvents: function (product) {
          var self = this;
          product.colors.on('click', function () {
            var color = $(this);
            var colorName = color.attr('data-color-name');
            self.updateProductView(product, colorName);
          });
      },
      updateProductView: function (product, colorName) {
        this.setColorName(product.colorName, colorName);
        this.setActiveElement(product.imgs, colorName);
        this.setActiveElement(product.colors, colorName);
        this.setActiveElement(product.shopNowBtns, colorName);
      },
      setActiveElement: function (elements, colorName) {
        var selectedElem = function (index, element) {
          return $(element).attr('data-color-name') == colorName;
        }
        elements.filter(selectedElem).addClass('active').siblings().removeClass('active');
      },
      setColorName: function(colorName, value) {
          colorName.find('span').html(value);
      }
  };

  function ProductItem(product) {
      this.imgs = product.find('.image img');
      this.colorName = product.find('.color-name');
      this.colors = product.find('.colored-products li');
      this.shopNowBtns = product.find('.buttons-wrapper .button');
  }

  selectProductByColorCtrl.init();
});